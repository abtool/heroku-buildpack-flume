#!/bin/sh
# Usage: bin/compile <build-dir> <cache-dir> <env-dir>

# fail fast
set -e

BUILDPACK_DIR=`cd $(dirname $0); cd ..; pwd`
BUILD_DIR=$1
CACHE_DIR=$2
ENV_DIR=$3

# settings
FLUME_PLUGINS=$BUILD_DIR/plugins.d
FLUME_CONF=$BUILD_DIR/flume.conf
FLUME_ENV=$BUILD_DIR/flume-env.sh
FLUME_VERSION=1.6.0
JAVA_VERSION=1.8

# install JDK
echo -n "-----> Installing OpenJDK ${JAVA_VERSION}..."
curl --silent --location http://heroku-jvm-common.s3.amazonaws.com/jvm-buildpack-common.tar.gz | tar xz
. bin/java
install_java ${BUILD_DIR} "${JAVA_VERSION}"
echo " done"
  
# install Flume
echo -n "-----> Installing Flume ${FLUME_VERSION}..."  
  
# cache Flume
mkdir -p $CACHE_DIR
cd $CACHE_DIR
FLUME_DIST=http://ftp.piotrkosoft.net/pub/mirrors/ftp.apache.org/flume/${FLUME_VERSION}/apache-flume-${FLUME_VERSION}-bin.tar.gz
FLUME_CACHED_DIR_NAME=apache-flume-${FLUME_VERSION}-bin
FLUME_EXT=$BUILDPACK_DIR/ext/links.txt
FLUME_LOGGING=$BUILDPACK_DIR/log4j.properties
if [ ! -d $FLUME_CACHED_DIR_NAME ]; then
	curl --silent --location $FLUME_DIST | tar xzm	
	(cd $FLUME_CACHED_DIR_NAME/lib;
		while read url ; do
			filename=`echo "${url##*/}"`
			libname=`echo "${filename%-*}"`
			find . -name "$libname-*.jar" -exec mv '{}' '{}'.old \;
			curl --silent -O $url > /dev/null
		done < $FLUME_EXT)
	cp $FLUME_LOGGING $FLUME_CACHED_DIR_NAME/conf/
fi

# add Flume runtime
cd $BUILD_DIR
FLUME_DIR_NAME=flume
if [ ! -d $FLUME_DIR_NAME ]; then
  cp -r $CACHE_DIR/$FLUME_CACHED_DIR_NAME $FLUME_DIR_NAME
fi
echo " done"

# configure Flume
echo -n "-----> Configuring Flume..."
if [ -d $FLUME_PLUGINS ]; then
  echo -n " +plugins"
  rm -rf $FLUME_DIR_NAME/plugins.d
  mv $FLUME_PLUGINS $FLUME_DIR_NAME/
fi
if [ -f $FLUME_CONF ]; then
  echo -n " +config"
  mv $FLUME_CONF $FLUME_DIR_NAME/conf/
fi
if [ -f $FLUME_ENV ]; then
  echo -n " +env"
  mv $FLUME_ENV $FLUME_DIR_NAME/conf/
  chmod a+x $FLUME_DIR_NAME/conf/flume-env.sh
else
  rm -f $FLUME_DIR_NAME/conf/flume-env.sh
fi
echo " done"