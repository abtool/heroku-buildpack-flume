# Heroku buildpack: Flume #

This is a Heroku buildpack to run Flume agent. The buildpack installs Open JDK 1.8 and Flume 1.6.0 by default. 

### Usage ###

    $ ls
    Procfile flume.conf plugins.d

    $ heroku config:set BUILDPACK_URL=https://bitbucket.org/abtool/heroku-buildpack-flume.git

    $ git push heroku master
    ...
    -----> Fetching custom git buildpack... done
    -----> Flume app detected
    -----> Installing OpenJDK 1.x... done
    -----> Installing Flume 1.x.x... done
    -----> Configuring Flume... +plugins +config +env done

The buildpack detects `flume.conf` in the project root folder. It also adds custom Flume `plugins.d` and `flume-env.sh` If available.

### Configuration ###

Sample Procfile for web process:

    web: flume/bin/flume-ng agent -c flume/conf -f flume/conf/flume.conf -n node1